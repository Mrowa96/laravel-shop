@extends("admin.layouts.master")

@section("content")
    Produkty <a href="{{ route("addItem") }}">Dodaj nowy</a>

    <ul>
        @foreach($items AS $item)
            <li>
                {{ $item->name }} <a href="{{ route("removeItem", ['group_id' => $item->id]) }}">Usuń</a>
            </li>
        @endforeach
    </ul>
@endsection