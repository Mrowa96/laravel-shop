@extends("admin.layouts.master")

@section("content")
    Kategorie <a href="{{ route("addGroup") }}">Dodaj nową</a>

    <ul>
        @foreach($groups AS $group)
            <li>
                {{ $group->name }} <a href="{{ route("removeGroup", ['group_id' => $group->id]) }}">Usuń</a>
            </li>
        @endforeach
    </ul>
@endsection