@extends("admin.layouts.master")

@section("content")
    Dodaj grupę

    {!! Form::open(array('url' => route('addGroup'), 'method' => 'post', 'class' => 'form', 'role' => 'form')) !!}
        <div class="form-group">
            {!! Form::text('name', null, ["class" => "form-control", "placeholder" => "Nazwa grupy"]) !!}

            {!! Form::submit('Dodaj') !!}
        </div>
    {!! Form::close() !!}
@endsection