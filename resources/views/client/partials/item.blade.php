@if($item)
    <div class="item">
        <a href="{{ route('item', ['id' => $item['id']]) }}">
            <span class="name">{{ $item['name'] }}</span>
        </a>
    </div>
@endif