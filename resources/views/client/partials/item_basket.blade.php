@if($item)
    <li class="item">
        <a href="{{ route('item', ['id' => $item['id'], 'url' => $item['url']]) }}">
            <span class="name">{{ $item['name'] }}</span>
        </a>

        <span class="price">{{ $item['price'] }} zł</span>
        <span class="quantity">{{ $item['quantity'] }} sztuk</span>

        {!! Form::open(array('url' => route('removeFromBasket'), 'method' => 'post', 'class' => 'form', 'role' => 'form')) !!}
        <div class="form-group">
            {!! csrf_field() !!}

            {!! Form::number('id', $item->id, ['hidden' => true]) !!}

            {!! Form::submit('Usun produkt') !!}
        </div>
        {!! Form::close() !!}
    </li>
@endif