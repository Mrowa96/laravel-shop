<div class="container-fluid" id="header">
    <span class="name">Sklep Internetowy</span>

    <a href="{{ route('home')}}" class="btnMy">
        <i class="fa fa-home"></i>
    </a>
    <a href="{{ route('search')}}" class="btnMy">
        <i class="fa fa-search"></i>
    </a>
    <a href="{{ route('basket')}}" class="btnMy">
        <i class="fa fa-shopping-cart"></i>
    </a>
    <a href="{{ route('user')}}" class="btnMy">
        <i class="fa fa-user"></i>
    </a>
    <a href="{{ route('admin')}}" class="btnMy">
        <i class="fa fa-tachometer"></i>
    </a>
</div>