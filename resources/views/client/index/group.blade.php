@extends("client.layouts.master")

@section("content")
    Kategoria - {{$group->name}}

    @if(count($items))
        @foreach($items AS $item)
            @include('client.partials.item', ['item' => $item])
        @endforeach
    @else
        <span>Brak produktów  w tej kategorii</span>
    @endif
@endsection