@extends("client.layouts.master")

@section("content")
    Produkt - {{$item->name}}
    Cena - {{$item->price}}

    {!! Form::open(array('url' => route('addToBasket'), 'method' => 'post', 'class' => 'form', 'role' => 'form')) !!}
    <div class="form-group">
        {!! csrf_field() !!}

        {!! Form::label('quantity', "Ilość") !!}
        {!! Form::number('quantity', 1) !!}
        {!! Form::number('price', $item->price, ['hidden' => true]) !!}
        {!! Form::number('id', $item->id, ['hidden' => true]) !!}

        {!! Form::submit('Dodaj do koszyka') !!}
    </div>
    {!! Form::close() !!}
@endsection