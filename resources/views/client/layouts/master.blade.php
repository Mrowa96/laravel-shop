<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <link href="{{ asset("styles/app.css") }}" rel="stylesheet">
        <link href="{{ asset("styles/font-awesome.min.css") }}" rel="stylesheet">
    </head>

    <body>
        <div id="wrapper" class="client">
            @include("client.partials.header")

            <div class="container-fluid">
                <div class="container">
                    <div class="row" id="content">
                        @yield("content")
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>