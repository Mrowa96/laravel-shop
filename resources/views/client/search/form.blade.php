@extends("client.layouts.master")

@section("content")
    Wyszukaj

    {!! Form::open(array('url' => route('searchResults'), 'method' => 'get', 'class' => 'form', 'role' => 'form')) !!}
        <div class="form-group">
            {!! Form::text('name', null, ["class" => "form-control", "placeholder" => "Nazwa produktu"]) !!}
            {!! Form::select('group', $groups) !!}
            {!! Form::number('price_from', null, ["class" => "form-control", "placeholder" => "Cena od", 'min' => 0]) !!}
            {!! Form::number('price_to', null, ["class" => "form-control", "placeholder" => "Cena do", 'min' => 0]) !!}

            {!! Form::submit('Szukaj') !!}
        </div>
    {!! Form::close() !!}
@endsection