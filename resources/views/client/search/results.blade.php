@extends("client.layouts.master")

@section("content")
    Wyniki wyszukiwania

    @if(empty($results))
        <span>Brak wyników</span>
    @else
        @foreach($results AS $item)
            @include("client.partials.item", ['item' => $item])
        @endforeach
    @endif
@endsection