@extends("client.layouts.master")

@section("content")
    Koszyk - Adres

    {!! Form::open(array('url' => route('basketAddressPost'), 'method' => 'post', 'class' => 'form', 'role' => 'form', 'id' => 'basketAddress')) !!}
    <div class="form-group">
        {!! csrf_field() !!}

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="form-group">
            <div class="col-lg-2">
                {!! Form::label("name", "Imię") !!}
            </div>

            <div class="col-md-10">
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-2">
                {!! Form::label("surname", "Nazwisko") !!}
            </div>

            <div class="col-md-10">
                {!! Form::text('surname', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        {!! Form::number('user_id', Auth::user()->id, ['hidden' => true]) !!}

        <span class="btn" id="sendAddress">Wybierz sposób dostawy</span>
        <span class="btn" id="frozeOrder">Ustaw adres i dokończ później</span>
    </div>
    {!! Form::close() !!}

    <script>
        var basketAddress = document.querySelector("#basketAddress"),
            sendAddress = document.querySelector("#sendAddress"),
            frozeOrder = document.querySelector("#frozeOrder");

        sendAddress.addEventListener("click", function(){
            basketAddress.submit();
        }, false);

        frozeOrder.addEventListener("click", function(){
            basketAddress.setAttribute("action", "/basket/froze-order");
            basketAddress.submit();
        }, false);
    </script>
@endsection