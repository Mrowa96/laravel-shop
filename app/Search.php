<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Search extends Model
{
    public static function searchItems($name = "", $group = 0, $priceFrom = 0, $priceTo = 0){
        $items = DB::table('items');

        if($group != 0){
            $items = $items->join('groups', 'groups.id', '=', 'items.group_id')->select('items.*','groups.id')->where('groups.id', '=', $group);
        }
        else{
            $items = $items->select('items.*');
        }

        if(!empty($name)){
            $items = $items->where('items.name', 'LIKE', "%".$name."%");
        }

        if($priceFrom != 0){
            $items = $items->where('items.price', '>', $priceFrom);
        }

        if($priceTo != 0){
            $items = $items->where('items.price', '<', $priceTo);
        }

        $itemsArr = $items->get();

        return $itemsArr;
    }
}
