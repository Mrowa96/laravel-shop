<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    protected $table = "users";
    protected $primaryKey = "id";
    protected $fillable = [
        'login', 'email', 'password',
    ];

    public function permissions(){
        return $this->belongsToMany('App\Permission')->withTimestamps();
    }
}
