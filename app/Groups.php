<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    protected $table = "groups";
    protected $primaryKey = "id";
    protected $fillable = ['name'];

    public static function prepareForSelect(){
        $groups = Groups::where("enable", "1")->get();
        $selects = [];

        $selects[0] = "Wybierz...";

        foreach($groups AS $group){
            $selects[$group->id] = $group->name;
        }

        return $selects;
    }
}
