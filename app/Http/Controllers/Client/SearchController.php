<?php

namespace App\Http\Controllers\Client;

use App\GroupItems;
use App\Groups;
use App\Http\Controllers\Controller;
use App\Items;
use App\Search;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SearchController extends Controller
{
    public function searchForm(){
        $groups = Groups::prepareForSelect();

        return view("client.search.form", ['groups' => $groups]);
    }

    public function searchResults(){
        $name = Input::get('name');
        $group = Input::get('group');
        $priceFrom = Input::get('price_from');
        $priceTo = Input::get('price_to');

        $results = Search::searchItems($name, $group, $priceFrom, $priceTo);

        return view("client.search.results", ['results' => $results]);

    }
}
