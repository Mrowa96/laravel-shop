<?php

namespace App\Http\Controllers\Client;

use App\GroupItems;
use App\Groups;
use App\Http\Controllers\Controller;
use App\Items;
use App\Search;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

class IndexController extends Controller
{
    public function homeAction(){
        $groups = Groups::all();

        return view("client.index.home", ['groups' => $groups]);
    }

    public function groupView(Request $request, $id){
        $group = Groups::find($id);
        $items = Items::where("group_id", $id)->get();

        if($group){
            return view("client.index.group", ['group' => $group, "items" => $items]);
        }
        else{
            return redirect()->action('Client\\IndexController@homeAction');
        }
    }

    public function itemView(Request $request, $id){
        $item = Items::find($id);

        if($item){
            return view("client.index.item", ['item' => $item]);
        }
        else{
            return redirect()->action('Client\\IndexController@homeAction');
        }
    }
}
