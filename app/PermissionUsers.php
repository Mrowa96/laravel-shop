<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionUsers extends Model
{
    protected $table = "permission_users";
    protected $primaryKey = "id";
    protected $fillable = ['permission_id', 'users_id'];
}
